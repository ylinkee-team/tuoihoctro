/**
 * Created by andh on 1/29/17.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    autoIncrement = require('mongoose-auto-increment'),
    config = require('../configs/config.js'),
    connection = mongoose.createConnection(config.database);
autoIncrement.initialize(connection);
// var random = require('mongoose-simple-random');
var ContentSchema = new Schema({
    title: {
        type: String,
        trim: true,
        required: 'Cấn tiêu đề',
        maxlength: 50
    },
    description: {
        type: String,
        trim: true,
        maxlength: 200
    },
    thumb: {
        type: String,
        trim: true,
        required: 'Cần thumb'
    },
    categories: [{
        type: String,
        required: 'Cần thể loại',
        ref: 'Category'
    }],
    shares: [{
        type: Number,
        ref: 'User',
        default: []
    }],
    follows: [{
        type: Number,
        ref: 'User',
        default: []
    }],
    popular: Number,
    trending: Number,
    created: {
        type: Date,
        default: Date.now
    },
    creator: {
        type: Number,
        ref: 'User',
        required: 'Cần người tạo'
    },
    type: {
        type: String,
        required: 'Cần type'
    },
    publish: {
        type: Boolean,
        default: false
    },
    review: {
        type: Boolean,
        default: false
    },
    reviewMessage: String
});
ContentSchema.plugin(autoIncrement.plugin, {
    model: 'Content',
    startAt: 1
});
// ContentSchema.statics.findChallengeByURL = function(url, callback) {
//     this.findOne({
//             url: url
//         }).populate('creator', 'displayName username avatar')
//         .populate('categories', 'title')
//         .populate('types', 'title')
//         .exec(callback);
// };
// GameSchema.plugin(random);
ContentSchema.index({ title: 'text', description: 'text' });
ContentSchema.set('toJSON', { getters: true, virtuals: true });
mongoose.model('Content', ContentSchema);