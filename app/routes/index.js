var passport = require('passport');
var config = require('../configs/config');
var users = require('../controllers/user.server.controller');

    /* GET home page. */
module.exports = function(app) {

   /////////// LOCAL REGISTER
    app.post('/register', users.register);

    /////////// LOCAL LOGIN
    app.post('/login',  users.login);

    /////////// LOGOUT
    app.get('/logout', users.authLogout);

    /////////// FACEBOOK LOGIN
    app.get('/oauth/facebook', function(req, res, next) {
        req.session.redirect = req.query.redirect || '/';
        next();
    }, passport.authenticate('facebook', { scope: ['user_friends', 'email', 'public_profile'] }));
    app.get('/oauth/facebook/callback', passport.authenticate('facebook', { failureRedirect: '/login' }), function(req, res) {
        res.redirect(req.session.redirect + "?token=" + req.user.token);
    });

    app.get('/', function(req, res, next) {
        res.render('index', { message: null, app: config.app, channel: config.server.channel });
    });

}