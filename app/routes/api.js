/**
 * Created by andh on 1/28/17.
 */
var passport = require('passport');
var users = require('../controllers/user.server.controller');
var contents = require('../controllers/content.server.controller');
var categories = require('../controllers/category.server.controller');

module.exports = function (router) {
    router.use(passport.authenticate('bearer', { session: false }));
    /* TOKEN */
    router.get('/token', users.authToken);

    //////////// CATEGORY
    router.post('/categories', categories.create)
        .get('/categories', categories.list)
    router.route('/categories/:categoryID')
        .get(categories.get)
        .put(users.requiresManager, categories.update);
    router.param('categoryID', categories.categoryByURL);

    //////////// CONTENT
     router.post('/contents', contents.create)
        .get('/contents', contents.list)
    router.route('/contents/:contentID')
    .delete(contents.remove);
    router.param('contentID', contents.contentByID);

};
