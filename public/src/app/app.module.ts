import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { ModalModule } from 'angular2-modal';
import { BootstrapModalModule } from 'angular2-modal/plugins/bootstrap';

import { AppComponent } from './app.component';
import { HotComponent } from './components/hot/hot.component';
import { HeroDetailComponent } from './components/hero-detail/hero-detail.component';
import { HeroService} from './services/hero.service';
import { HeroesComponent } from './components/heroes/heroes.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { AppRoutingModule }     from './app-routing.module';
import { FooterComponent } from './components-shared/footer/footer.component';
import { HeaderComponent } from './components-shared/header/header.component';

import { CustomModal} from './directives/custom-modal/custom-modal'

@NgModule({
  declarations: [
    AppComponent,
    HotComponent,
    HeroDetailComponent,
    HeroesComponent,
    DashboardComponent,
    CustomModal,
    FooterComponent,
    HeaderComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule,
    ModalModule.forRoot(),
    BootstrapModalModule
  ],
  providers: [HeroService],
  bootstrap: [AppComponent],
  entryComponents: [ CustomModal ]
})
export class AppModule { }
