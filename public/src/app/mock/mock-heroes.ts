import { Hero } from '../class/hero';

export const HEROES: Hero[] = [
  {id: 11, name: 'Mr. Nice',point: 11},
  {id: 12, name: 'Narco',point: 21},
  {id: 13, name: 'Bombasto',point: 41},
  {id: 14, name: 'Celeritas',point: 42},
  {id: 15, name: 'Magneta',point: 121},
  {id: 16, name: 'RubberMan',point: 541},
  {id: 17, name: 'Dynama',point: 235},
  {id: 18, name: 'Dr IQ',point: 1234},
  {id: 19, name: 'Magma',point: 2341},
  {id: 20, name: 'Tornado',point: 12}
];
