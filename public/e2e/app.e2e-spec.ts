import { TeenStoryPage } from './app.po';

describe('teen-story App', function() {
  let page: TeenStoryPage;

  beforeEach(() => {
    page = new TeenStoryPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
